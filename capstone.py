# Capstone Development
# Python Object Oriented Programming

from abc import ABC

# Person Class
class Person(ABC):

        def __init__(self, firstName, lastName, email, department):
            super().__init__()
            self._firstName = firstName
            self._lastName = lastName
            self._email = email
            self._department = department

        # Getter

        def getFirstName(self):
            print (f"{self._firstName}")

        def getLastName(self):
            print (f"{self._lastName}")

        def getFullName(self):
            return f"{self._firstName} {self._lastName}"

        # Setter

        def setFirstName(self, new_firstName):
            self._firstName = new_firstName

        def setLastName(self, new_lastName):
            self._lastName = new_lastName

        # Methods

        def login(self):
            return (f"{self._email} has logged in!")

        def logout(self):
            return (f"{self._email} has logged out!")

        def addRequest(self):
            return "Request has been added!"

        def checkRequest(self):
            pass

        def addUser(self):
            pass

# Employee Class
class Employee(Person):
        def __init__(self, firstName, lastName, email, department):
            # super().__init__(firstName, lastName, email, department)
            self._firstName = firstName
            self._lastName = lastName
            self._email = email
            self._department = department
            self._users = []

        # Getter

        def getFirstName(self):
            return self._firstName

        def getLastName(self):
            return self._lastName

        def getEmail(self):
            return self._email

        def getDepartment(self):
            return self._department

        # Setter

        def setFirstName(self, firstName):
            self._firstName = firstName
            
        
        def setLastName(self, lastName):
            self._lastName = lastName
            
       
        def setEmail(self, email):
            self._email = email
            
        
        def setDepartment(self, department):
            self._department = department

        # Methods

        def getFullName(self):
            return f"{self._firstName} {self._lastName}"

        def addRequest(self, request):
            self.requests.append(request)

        def checkRequest(self):
            return "Not Available for regular employees"
            pass

        def addUser(self, user):
            return "Not Available for regular employees"
            pass

        def login(self):
            return f"{self._email} has logged in"

        def logout(self):
            return f"{self._email} has logged out"


        def checkRequest(self):
            pass 
        
        def addUser(self):
            pass

# TeamLead Class
class TeamLead(Person):
        def __init__(self, firstName, lastName, email, department):
            super().__init__(firstName, lastName, email, department)

            self._members = []
        
        def checkRequest(self):
            pass 
        
        def addUser(self):
            pass

        def addMember(self, member):
            self._members.append(member)


        def getMembers(self):
            return self._members

# Admin Class
class Admin(Person):
        def __init__(self, firstName, lastName, email, department):
            super().__init__(firstName, lastName, email, department)

            self._firstName = firstName
            self._lastName = lastName
            self._email = email
            self._department = department
            self._users = []

        
        def checkRequest(self):
            pass

        def addUser(self):
            return (f"User has been added!")

class Request:
        def __init__(self, name, requester, dateRequested):

            self._name = name
            self._requester = requester
            self._dateRequested = dateRequested
            self._status = "Open"

        def updateRequest(self, newStatus):
            self._status = newStatus

        def closeRequest(self):
            self._status = "Closed"
            return f"Request Laptop repair has been {self._status}"

        def cancelRequest(self):
            self._status = "Cancelled"


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
# assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

# assert admin1.addUser() == "User has been added"

# req2.set_status("closed")
print(req2.closeRequest())
